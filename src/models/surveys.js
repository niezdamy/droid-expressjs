let surveyQuestions = {
  1: {
    id: "1",
    text: "Are you a droid?",
    goodAnswerResult: "yes",
    nextQuestion: 2,
    badAnswer: 1
  },
  2: {
    id: "2",
    text: "So then, prove you can do some math. What is the sum of 8 and 9?",
    goodAnswerResult: "17",
    nextQuestion: 3,
    badAnswer: 4
  },
  3: {
    id: "3",
    text: "You are right! Wanna try another one?",
    goodAnswerResult: "yes",
    nextQuestion: 4,
    badAnswer: 3
  },
  4: {
    id: "4",
    rand1: Math.floor(Math.random() * 10),
    rand2: Math.floor(Math.random() * 10),
    get text() {
      return `What is sum ${this.rand1} and ${this.rand2}`;
    },
    get goodAnswerResult() {
      return this.rand1 + this.rand2;
    },
    get reset() {
      this.rand1 = Math.floor(Math.random() * 10);
      this.rand2 = Math.floor(Math.random() * 10);
    },
    nextQuestion: 6,
    badAnswer: 2
  },
  5: {
    id: "5",
    text: "​Let’s try again. What is the sum of 8 and 9?",
    goodAnswerResult: "17",
    nextQuestion: 3,
    badAnswer: 1
  },
  6: {
    id: "6",
    text: "You are right! I’ll remember you can do the maths!",
    lastQuestion: true
  }
};

let surveyAnswers = {
  1: {
    id: "1",
    text: "That’s so sad",
  },
  2: {
    id: "2",
    text: "I see... Nice try, human",
  },
  3: {
    id: "3",
    text: "Nice try, human",
  },
  4: {
      id: "4",
      text: "Let’s try again. What is the sum of 8 and 9?",
      goodAnswerResult: "17",
  }
};

export default {
  surveyQuestions,
  surveyAnswers
};
