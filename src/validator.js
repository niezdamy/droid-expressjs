import * as logService from "./services/logService";

export function checkIfThereAreErrorsForDroidRequest(droidId, message) {
  if (logService.checkIfDroidPassTestInPast(droidId)) {
    return `Droid with id: ${droidId} passed test already, You can't pass it again, sorry.`;
  }

  const parsedInt = parseInt(message);
  if (parsedInt > 100 || parsedInt < 0) {
    return `Value: ${parsedInt} is out of range 0-100, sorry.`;
  }

  if (message == undefined) {
    return `Message is empty`;
  }

  const allowedResponses = ["yes", "no", "hello"];
  if (!parsedInt && !allowedResponses.includes(message.toLowerCase())) {
    return `Message: ${message} is out of range of possible answers (Hello, Yes, No, [0-100], sorry)`;
  }
}
