import "dotenv/config";
import express from "express";
import * as logService from "./services/logService";
import * as surveyService from "./services/surveyService";
import * as validator from "./validator";

export const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.listen(process.env.PORT, () =>
  console.log(`App is listening on port ${process.env.PORT}!`)
);

app.get("/allLogs", (req, res) => {
  const allLogs = logService.getAllLogs();
  return res.send(allLogs);
});

app.get("/logsForDroid", (req, res) => {
  const { droidId, message } = req.body;

  if (!logService.checkIfDayOfFirstLogIsCorrectForDroidId(droidId, message)) {
    return res
      .status(400)
      .send({ errorMessage: "Incorrect first day of logs value or droidId" });
  }
  const logs = logService.getLogsForDroid(droidId, message);
  return res.send(logs);
});

app.post("/survey", (req, res) => {
  const { droidId, message } = req.body;
  var errorMessage = validator.checkIfThereAreErrorsForDroidRequest(droidId, message);
  if (errorMessage) {
    return res.status(400).send({ errorMessage });
  }

  logService.logRequest(req.body);
  const applicationResponse = surveyService.takeSurvey(
    droidId,
    message.toLowerCase()
  );
  logService.logApplicationResponse(applicationResponse);

  return res.send({ response: applicationResponse.message });
});
