import logs from "../models/logs";
import uuidv4 from "uuid/v4";

let logBus = logs.logsArray;

export function logRequest(request) {
  let requestToLog = this.setAuditData(request);
  requestToLog.type = "REQUEST";
  logBus = [...logBus, requestToLog];
}

export function logApplicationResponse(response) {
  let responseToLog = this.setAuditData(response);
  responseToLog.type = "APP_RESPONSE";
  logBus = [...logBus, responseToLog];
}

export function setAuditData(data) {
  data.logId = uuidv4();
  data.logDate = new Date(Date.now());
  return data;
}

export function addDroidToPassed(droidId) {
  logs.droidPassedArrays.push(droidId);
}
export function checkIfDroidPassTestInPast(droidId) {
  if (logs.droidPassedArrays.indexOf(droidId) != -1) 
  return true;
}

export function getLastQuestionIdForDroid(droidId) {
  const questionIdArray = logBus
    .filter(x => x.droidId === droidId)
    .filter(y => y.questionId)
    .map(x => x.questionId);
  return questionIdArray.pop();
}

export function getLastGoodAnswerForDroid(droidId) {
  const goodAnswerArray = logBus
    .filter(x => x.droidId === droidId)
    .filter(y => y.goodAnswerResult)
    .map(x => x.goodAnswerResult);
  return goodAnswerArray.pop();
}

export function getLogsForDroid(droidId) {
  const result = logBus
    .filter(x => x.droidId === droidId)
    .map(x => x.message);
  return result;
}

export function checkIfDayOfFirstLogIsCorrectForDroidId(droidId, day) {
  const result = logBus
    .filter(x => x.droidId === droidId)
    .map(x => new Date(x.logDate).getDate());
  if (result.shift() == day){
    return true;
  }
  return false;
}

export function getAllLogs() {
  return logBus;
}
