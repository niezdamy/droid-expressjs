import surveys from "../models/surveys";
import * as LogService from "./logService";

const questions = surveys.surveyQuestions;
const answers = surveys.surveyAnswers;

export function takeSurvey(droidId, message) {
  if (message === "hello") {
    return createResponseFromQuestion(droidId, 1);
  }

  const lastQuestionId = LogService.getLastQuestionIdForDroid(droidId);
  const goodAnswer = LogService.getLastGoodAnswerForDroid(droidId);

  if (lastQuestionId > 0) {
    if (message == goodAnswer) {
      return createResponseFromQuestion(
        droidId,
        questions[lastQuestionId].nextQuestion
      );
    }
    return createResponseFromAnswer(
      droidId,
      questions[lastQuestionId].badAnswer
    );
  }
  return {};
}

function createResponseFromQuestion(droidId, questionId) {
  const { id, text, goodAnswerResult } = questions[questionId];
  const response = {
    questionId: id,
    message: text,
    droidId: droidId,
    goodAnswerResult: goodAnswerResult
  };
  takeAditionalAction(droidId, questions[questionId]);
  return response;
}

function createResponseFromAnswer(droidId, answerId) {
  const { id, text, goodAnswerResult } = answers[answerId];
  let response = {
    answerId: id,
    message: text,
    droidId: droidId,
    goodAnswerResult: goodAnswerResult
  };

  return response;
}

function takeAditionalAction(droidId, question) {
  //generate new random numbers for future queries
  if (question.reset) {
    question.reset();
  }

  if (question.lastQuestion) {
    LogService.addDroidToPassed(droidId);
  }
}
