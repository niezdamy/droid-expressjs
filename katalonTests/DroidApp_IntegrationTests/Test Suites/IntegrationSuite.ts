<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>IntegrationSuite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a7a149f2-8ed6-4f51-b7f0-db28bac51ab9</testSuiteGuid>
   <testCaseLink>
      <guid>be93f321-5f25-4a21-a94e-59250555d0ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ValidationTests</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a44471ec-c7dd-4eba-a974-4078e04ccc3e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/HappyPathToPassDroidTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>97a96f6e-c11c-4d48-a7f9-df84fbb3e5ab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TheSameDroidShouldntPassTestAgain</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
