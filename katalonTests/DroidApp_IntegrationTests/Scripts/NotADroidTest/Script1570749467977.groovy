import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import groovy.json.JsonSlurper as JsonSlurper

def response1 = WS.sendRequest(findTestObject('Droid_hello'))

WS.verifyElementPropertyValue(response1, 'response', 'Are you a droid?')

def response2 = WS.sendRequest(findTestObject('Droid_answer_no'))

WS.verifyElementPropertyValue(response2, 'response', 'That’s so sad')

