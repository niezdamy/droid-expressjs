import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import groovy.json.JsonSlurper as JsonSlurper

KeywordLogger log = new KeywordLogger()

def response1 = WS.sendRequest(findTestObject('Droid_hello'))

WS.verifyElementPropertyValue(response1, 'errorMessage', 'Droid with id: 0c2e32dc-7625-4ef8-ba4b-9ed8c9b66fbe passed test already, You can\'t pass it again, sorry.')