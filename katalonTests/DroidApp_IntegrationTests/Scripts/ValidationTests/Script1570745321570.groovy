import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import groovy.json.JsonSlurper as JsonSlurper

def response1 = WS.sendRequest(findTestObject('Droid_number_outOfRange'))

WS.verifyElementPropertyValue(response1, 'errorMessage', 'Value: 123 is out of range 0-100, sorry.')

def response2 = WS.sendRequest(findTestObject('Droid_text_outOfRange'))

WS.verifyElementPropertyValue(response2, 'errorMessage', 'Message: hi is out of range of possible answers (Hello, Yes, No, [0-100], sorry)')

