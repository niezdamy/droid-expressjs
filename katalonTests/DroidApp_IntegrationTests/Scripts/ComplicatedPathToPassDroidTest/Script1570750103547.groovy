import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import groovy.json.JsonSlurper as JsonSlurper

def extractInts( String input ) {
	input.findAll( /\d+/ )*.toInteger()
  }

KeywordLogger log = new KeywordLogger()

def response1 = WS.sendRequest(findTestObject('Droid_hello'))

WS.verifyElementPropertyValue(response1, 'response', 'Are you a droid?')

def response2 = WS.sendRequest(findTestObject('Droid_answer_yes'))

WS.verifyElementPropertyValue(response2, 'response', 'So then, prove you can do some math. What is the sum of 8 and 9?')

def response3 = WS.sendRequest(findTestObject('Droid_answer_12'))

WS.verifyElementPropertyValue(response3, 'response', 'Let’s try again. What is the sum of 8 and 9?')

def response4 = WS.sendRequest(findTestObject('Droid_answer_17'))

WS.verifyElementPropertyValue(response4, 'response', 'You are right! Wanna try another one?')

def response5 = WS.sendRequest(findTestObject('Droid_answer_yes'))

def jsonSlurper = new JsonSlurper() 
def objectJsonResponse5 = jsonSlurper.parseText(response5.getResponseText())

log.logInfo(objectJsonResponse5.response)
def intArray = extractInts(objectJsonResponse5.response)
def calculatedSum = intArray.sum().toString()

def response6 = WS.sendRequest(findTestObject('Droid_answer_parametrized',[('sumParameter'): calculatedSum]))
WS.verifyElementPropertyValue(response6, 'response', 'You are right! I’ll remember you can do the maths!')

