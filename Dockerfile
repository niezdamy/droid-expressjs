FROM node:latest

COPY package.json /nodejs/package.json

WORKDIR /nodejs

RUN npm install

EXPOSE 3000:3000